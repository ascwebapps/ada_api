require "application_system_test_case"

class ShuttlesTest < ApplicationSystemTestCase
  setup do
    @shuttle = shuttles(:one)
  end

  test "visiting the index" do
    visit shuttles_url
    assert_selector "h1", text: "Shuttles"
  end

  test "creating a Shuttle" do
    visit shuttles_url
    click_on "New Shuttle"

    fill_in "Company", with: @shuttle.company
    fill_in "Max people", with: @shuttle.max_people
    fill_in "Name", with: @shuttle.name
    fill_in "People", with: @shuttle.people
    fill_in "Time", with: @shuttle.time
    click_on "Create Shuttle"

    assert_text "Shuttle was successfully created"
    click_on "Back"
  end

  test "updating a Shuttle" do
    visit shuttles_url
    click_on "Edit", match: :first

    fill_in "Company", with: @shuttle.company
    fill_in "Max people", with: @shuttle.max_people
    fill_in "Name", with: @shuttle.name
    fill_in "People", with: @shuttle.people
    fill_in "Time", with: @shuttle.time
    click_on "Update Shuttle"

    assert_text "Shuttle was successfully updated"
    click_on "Back"
  end

  test "destroying a Shuttle" do
    visit shuttles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Shuttle was successfully destroyed"
  end
end
