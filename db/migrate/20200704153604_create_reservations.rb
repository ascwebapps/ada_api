class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.references :shuttle, null: false, foreign_key: true
      t.integer :people
      t.string :telephone
      t.string :name
      t.string :surname

      t.timestamps
    end
  end
end
