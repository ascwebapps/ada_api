class CreateShuttles < ActiveRecord::Migration[6.0]
  def change
    create_table :shuttles do |t|
      t.string :name
      t.string :company
      t.integer :max_people
      t.integer :people
      t.time :time

      t.timestamps
    end
  end
end
