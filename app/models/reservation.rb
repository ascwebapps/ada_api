class Reservation < ApplicationRecord
  belongs_to :shuttle
  validates :name, presence: true
  validates :surname, presence: true
  validates :telephone, presence: true
  validates :people, presence: true
end
