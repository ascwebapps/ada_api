json.extract! shuttle, :id, :name, :company, :max_people, :people, :time, :created_at, :updated_at
json.url shuttle_url(shuttle, format: :json)
