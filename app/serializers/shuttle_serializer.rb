class ShuttleSerializer < ActiveModel::Serializer
  attributes :id, :name, :company, :max_people, :people, :time
end
