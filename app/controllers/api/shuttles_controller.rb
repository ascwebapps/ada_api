class Api::ShuttlesController < ApplicationController
  before_action :set_shuttle,  only: [:show]

  def index
    render json: Shuttle.all
  end

  def show
    render json: @shuttle
  end

  private

  def set_shuttle
    @shuttle = Shuttle.find(params[:id])
  end

end
