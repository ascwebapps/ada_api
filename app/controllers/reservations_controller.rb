class ReservationsController < ApplicationController
  include HTTParty
  def create
    @reservation = Reservation.new(reservation_params)
    if @reservation.save
      @reservation.shuttle.update(people: @reservation.shuttle.people + @reservation.people)
      response = HTTParty.post('https://hackathon.tim.it/sms/mt',
      headers:{
         "apikey": Rails.application.credentials.tim_api,
         "Content-Type": "application/json",
       },
      body: {
          "address": "tel:#{@reservation.telephone}",
          "message": "Prenotazione effettuata a nome di #{@reservation.name} #{@reservation.surname} per la navetta #{@reservation.shuttle.name} all'orario #{@reservation.shuttle.time.strftime("%H:%M")}. Posti prenotati: #{@reservation.people}. Numero prenotazione NR#{rand 2000..6000}"
      }.to_json)
      puts response.body, response.code, response.message, response.headers.inspect
      redirect_to new_reservation_path, notice: 'Grazie per la sua richiesta di prenotazione! Riceverai un sms di conferma della prenotazione'
    else
      flash[:notice] = 'Non è stato possibile prenotare'
      render "new"
    end
  end

  def new
    @reservation = Reservation.new
  end

private
  def reservation_params
    params.require(:reservation).permit(:name, :surname, :shuttle_id, :telephone, :people)
  end
end
