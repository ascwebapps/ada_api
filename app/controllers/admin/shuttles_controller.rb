class Admin::ShuttlesController < Admin::BaseController
  before_action :set_shuttle, only: [:show, :edit, :update, :destroy]

  # GET /shuttles
  # GET /shuttles.json
  def index
    @shuttles = Shuttle.all
  end

  # GET /shuttles/1
  # GET /shuttles/1.json
  def show
  end

  # GET /shuttles/new
  def new
    @shuttle = Shuttle.new
  end

  # GET /shuttles/1/edit
  def edit
  end

  # POST /shuttles
  # POST /shuttles.json
  def create
    @shuttle = Shuttle.new(shuttle_params)

    respond_to do |format|
      if @shuttle.save
        format.html { redirect_to [:admin, @shuttle], notice: 'Shuttle was successfully created.' }
        format.json { render :show, status: :created, location: @shuttle }
      else
        format.html { render :new }
        format.json { render json: @shuttle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shuttles/1
  # PATCH/PUT /shuttles/1.json
  def update
    respond_to do |format|
      if @shuttle.update(shuttle_params)
        format.html { redirect_to [:admin, @shuttle], notice: 'Shuttle was successfully updated.' }
        format.json { render :show, status: :ok, location: @shuttle }
      else
        format.html { render :edit }
        format.json { render json: @shuttle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shuttles/1
  # DELETE /shuttles/1.json
  def destroy
    @shuttle.destroy
    respond_to do |format|
      format.html { redirect_to admin_shuttles_url, notice: 'Shuttle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shuttle
      @shuttle = Shuttle.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def shuttle_params
      params.require(:shuttle).permit(:name, :company, :max_people, :people, :time)
    end
end
