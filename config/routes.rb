Rails.application.routes.draw do
  root "reservations#new"
  get 'reservations/new'
  resources :reservations, only: [:new, :create]
  resources :shuttles
    namespace :api do
      resources :shuttles
    end

  namespace :admin do
    root to: "dashboard#index"
    resources :shuttles
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
